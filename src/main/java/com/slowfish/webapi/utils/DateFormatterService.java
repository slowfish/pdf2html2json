package com.slowfish.webapi.utils;

import java.text.SimpleDateFormat;

public interface DateFormatterService {

    SimpleDateFormat getyyyyMMdd();

}
