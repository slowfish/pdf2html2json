package com.slowfish.webapi.dao;

import com.slowfish.webapi.model.PdfInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PdfFileDao extends MongoRepository<PdfInfo, Long> {

}
