package com.slowfish.webapi.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface PdfTransferSheetService {
    File transfer(MultipartFile pdfFile, String name) throws IOException, InterruptedException;
}
