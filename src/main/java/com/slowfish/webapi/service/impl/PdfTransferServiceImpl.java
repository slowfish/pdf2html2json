package com.slowfish.webapi.service.impl;

import com.aliyun.oss.OSSClient;
import com.slowfish.core.ContentConverter;
import com.slowfish.core.TableDetector;
import com.slowfish.core.content.Content;
import com.slowfish.core.impl.ContentConverterImpl;
import com.slowfish.core.impl.TableConverterImpl;
import com.slowfish.core.impl.TableDetectorImpl;
import com.slowfish.core.tables.RectTable;
import com.slowfish.core.tables.Table;
import com.slowfish.webapi.model.*;
import com.slowfish.webapi.service.AliOssUploadService;
import com.slowfish.webapi.service.PdfFileService;
import com.slowfish.webapi.service.PdfTransferService;
import com.slowfish.webapi.utils.DateFormatterService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PdfTransferServiceImpl implements PdfTransferService {

    @Autowired
    private PdfFileService pdfFileService;
    @Autowired
    private AliOssUploadService aliOssUploadService;
    @Autowired
    private DateFormatterService dateFormatterService;

//    private String jarPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

    @Value("${slowfish.storage.location}")
    private String storageLocation;
    @Value("${oss.bucket}")
    private String bucketName;
    @Value("${oss.image.host}")
    private String replaceHost;

//    @Override
//    public PdfInfo transfer(MultipartFile pdfFile, Long id, String code, String name) throws IOException {
//        try {
//            PdfInfo PdfInfo = null;
//            // screen shots
//            File file2 = new File(storageLocation + "/" + pdfFile.getOriginalFilename());
//            FileUtils.writeByteArrayToFile(file2, pdfFile.getBytes());
//            List<PdfScreenShot> screenShotsList = getScreenShots(file2, code + "_" + dateFormatterService.getyyyyMMdd().format(new Date()));
//            // covert pdf 2 html
//            File htmlFile = convertPdf2Html(pdfFile);
//            PdfInfo = (null == htmlFile) ? new PdfInfo() : transferByHtmlFile(htmlFile, code, name);
//            PdfInfo.setId(id);
//            PdfInfo.setScreenShots(screenShotsList);
//            return pdfFileService.add(PdfInfo);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public PdfInfo transfer(MultipartFile pdfFile, Long id, String code, String name) throws IOException {
        PdfInfo PdfInfo = null;
        // screen shots
        File file2 = new File(storageLocation + "/" + pdfFile.getOriginalFilename());
        FileUtils.writeByteArrayToFile(file2, pdfFile.getBytes());
        List<PdfScreenShot> screenShotsList = getScreenShots(file2, code + "_" + dateFormatterService.getyyyyMMdd().format(new Date()));
        // covert pdf 2 html
        PdfInfo = transferByPdfBox(file2, code, name);
        PdfInfo.setId(id);
        PdfInfo.setScreenShots(screenShotsList);
        return pdfFileService.add(PdfInfo);
    }

    // update 2018-05-15 换 PdfBox 进行解析
    private PdfInfo transferByPdfBox(File pdfFile, String code, String name) throws IOException {
        PDDocument document = PDDocument.load(pdfFile);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        // 读文本内容
        PDFTextStripper stripper = new PDFTextStripper();
        // 设置按顺序输出
        stripper.setSortByPosition(true);
        stripper.setStartPage(1);
        stripper.setEndPage(document.getNumberOfPages());
        String content = stripper.getText(document);
//        System.out.println(content);

        List<PdfContent> pdfContents = new ArrayList<>();
        PdfContent pdfContent = new PdfContent(content, new PdfPage(1L, (long) document.getNumberOfPages()));
        pdfContents.add(pdfContent);
        PdfInfo PdfInfo = new PdfInfo();
        PdfInfo.setBasicInfo(new PdfBasicInfo(code, name, new Date()));
        PdfInfo.setTables(new ArrayList<>());
        PdfInfo.setContents(pdfContents);
        return PdfInfo;
    }

    private File convertPdf2Html(MultipartFile pdfFile) throws IOException, InterruptedException {
        String fileName = "" + new Date().getTime();
        String srcFileName = storageLocation + "/" + fileName + ".pdf";
//        String targetName = jarPath + fileName + ".html";
//        String targetName = storageLocationTemp + "/" + fileName + ".html"; // 存储在当前 jar 目录下
        String targetName = fileName + ".html"; // 存储在当前 jar 目录下
        File pdfFile2 = new File(srcFileName);
        FileUtils.writeByteArrayToFile(pdfFile2, pdfFile.getBytes());
        String execLine[] = {"pdf2htmlEX", srcFileName, targetName};
        Process proc = Runtime.getRuntime().exec(execLine);
        int result = proc.waitFor();
        if (result < 0)
            return null;    // 程序执行失败
        File htmlFile = new File(targetName);
        return htmlFile.exists() ? htmlFile : null;
    }

    // 没法截屏，请调用外部的 pdf 方法
    private PdfInfo transferByHtmlFile(File htmlFile, String code, String name) throws IOException {
//        File file2 = new File(htmlFile.getOriginalFilename());
//        FileUtils.writeByteArrayToFile(file2, htmlFile.getBytes());
        Document document = Jsoup.parse(htmlFile, "utf-8");
        Element pages = document.body().getElementById("page-container");

        PdfInfo PdfInfo = new PdfInfo();
        PdfInfo.setBasicInfo(new PdfBasicInfo(code, name, new Date()));

        // table
        TableDetector detector = new TableDetectorImpl();
        List<RectTable> rectTables = detector.detect(pages.children());
        List<Table> tables = new TableConverterImpl().process(rectTables);
        List<PdfTable> pdfTables = new ArrayList<>();
        for (Table table : tables) {
            String[] names = table.getName();
            StringBuffer bf = new StringBuffer();
            if (null != names) {
                bf.append(names[0]);
                if (names.length > 1)
                    bf.append("  " + names[1]);
            }
            PdfTable pdfTable = new PdfTable(bf.toString().trim(), new PdfPage(table.getStartPageNumber(), table.getEndPageNumber()));
            pdfTable.setValues(table.getJsonTable());
            pdfTable.setHeaders(table.getHeaders());
            pdfTable.setKeys(table.getKeys());
            pdfTables.add(pdfTable);
        }
        // content
        ContentConverter contentConverter = new ContentConverterImpl();
        List<Content> contents = contentConverter.convert(pages.children());
        List<PdfContent> pdfContents = new ArrayList<>();
        for (Content content : contents) {
            PdfContent pdfContent = new PdfContent(content.getContentText(), new PdfPage(content.getStartPageNumber(), content.getEndPageNumber()));
            pdfContents.add(pdfContent);
        }
        PdfInfo.setTables(pdfTables);
        PdfInfo.setContents(pdfContents);
//        PdfInfo.setScreenShots(screenShotsList);
//        PdfInfo.setId(Long.parseLong(code + "" + System.currentTimeMillis() / 1523 + "" + (int) (1000 * Math.random())));
        return PdfInfo;
    }

    @Override
    public List<PdfScreenShot> getScreenShots(File pdfFile, String folderName) throws IOException {
        List<BufferedImage> images = convertToImage(pdfFile);
        // upload
        List<PdfScreenShot> screenShots = new ArrayList<>();
        OSSClient client = aliOssUploadService.getOSSClient();
        int currentPageCount = 1;
        for (BufferedImage image : images) {
            String fileName = UUID.randomUUID().toString() + ".jpg";
            String filePath = storageLocation + "/" + fileName;
            File outputfile = new File(filePath);
            ImageIO.write(image, "jpg", outputfile);
            String url = aliOssUploadService.uploadObject2OSS(client, outputfile, bucketName, folderName, fileName);
            url = url.replace("http://aimidas-test.oss-cn-beijing.aliyuncs.com", replaceHost);
            PdfScreenShot screenShot = new PdfScreenShot(currentPageCount, url);
            screenShots.add(screenShot);
            currentPageCount++;
        }
        return screenShots;
    }

    private List<BufferedImage> convertToImage(File file) throws IOException {
        PDDocument document = PDDocument.load(file);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        List<BufferedImage> bufferedImageList = new ArrayList<>();
        for (int page = 0; page < document.getNumberOfPages(); page++) {
            BufferedImage img = pdfRenderer.renderImageWithDPI(page, 96, ImageType.RGB);
            bufferedImageList.add(img);
        }
        document.close();

        return bufferedImageList;
    }
}
