package com.slowfish.webapi.service.impl;

import com.slowfish.webapi.dao.PdfFileDao;
import com.slowfish.webapi.model.PdfInfo;
import com.slowfish.webapi.service.PdfFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 把今天最好的表现当作明天最新的起点．．～
 * いま 最高の表現 として 明日最新の始発．．～
 * Today the best performance  as tomorrow newest starter!
 * Created by IntelliJ IDEA.
 **/

@Service
public class PdfFileServiceImpl implements PdfFileService {
    private final PdfFileDao dao;

    @Autowired
    public PdfFileServiceImpl(PdfFileDao dao) {
        this.dao = dao;
    }

    @Override
    public List<PdfInfo> findAll() {
        return dao.findAll();
    }

    @Override
    public PdfInfo findById(Long id) {
//        Optional<PdfInfo> pdfFile = dao.findById(id);
//        return pdfFile.get();
        PdfInfo pdfFile = dao.findOne(id);
        return pdfFile;
    }

    @Override
    public PdfInfo add(PdfInfo mongoUser) {
        return dao.save(mongoUser);
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    public PdfInfo update(PdfInfo mongoUser) {
        return dao.save(mongoUser);
    }
}
