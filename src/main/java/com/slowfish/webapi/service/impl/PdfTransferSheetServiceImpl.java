package com.slowfish.webapi.service.impl;

import com.slowfish.core.TableDetector;
import com.slowfish.core.impl.TableConverterImpl;
import com.slowfish.core.impl.TableDetectorImpl;
import com.slowfish.core.tables.RectTable;
import com.slowfish.core.tables.Table;
import com.slowfish.webapi.model.PdfPage;
import com.slowfish.webapi.model.PdfTable;
import com.slowfish.webapi.service.PdfTransferSheetService;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PdfTransferSheetServiceImpl implements PdfTransferSheetService {

    @Value("${slowfish.storage.location}")
    private String storageLocation;

    @Override
    public File transfer(MultipartFile pdfFile, String name) throws IOException, InterruptedException {
        File htmlFile = convertPdf2Html(pdfFile);
        List<PdfTable> tables = transferByHtmlFile(htmlFile);
        return tables2Sheet(tables, name == null ? "" : name);
    }

    // 1
    private File convertPdf2Html(MultipartFile pdfFile) throws IOException, InterruptedException {
        String fileName = "" + new Date().getTime();
        String srcFileName = storageLocation + "/" + fileName + ".pdf";
//        String targetName = jarPath + fileName + ".html";
//        String targetName = storageLocationTemp + "/" + fileName + ".html"; // 存储在当前 jar 目录下
        String targetName = fileName + ".html"; // 存储在当前 jar 目录下
        File pdfFile2 = new File(srcFileName);
        FileUtils.writeByteArrayToFile(pdfFile2, pdfFile.getBytes());
        String execLine[] = {"pdf2htmlEX", srcFileName, targetName};
        Process proc = Runtime.getRuntime().exec(execLine);
        int result = proc.waitFor();
        if (result < 0)
            return null;    // 程序执行失败
        File htmlFile = new File(targetName);
        return htmlFile.exists() ? htmlFile : null;
    }

    // 2
    private List<PdfTable> transferByHtmlFile(File htmlFile) throws IOException {
        Document document = Jsoup.parse(htmlFile, "utf-8");
        Element pages = document.body().getElementById("page-container");

        // table
        TableDetector detector = new TableDetectorImpl();
        List<RectTable> rectTables = detector.detect(pages.children());
        List<Table> tables = new TableConverterImpl().process(rectTables);
        List<PdfTable> pdfTables = new ArrayList<>();
        for (Table table : tables) {
            String[] names = table.getName();
            StringBuffer bf = new StringBuffer();
            if (null != names) {
                bf.append(names[0]);
                if (names.length > 1)
                    bf.append("  " + names[1]);
            }
            PdfTable pdfTable = new PdfTable(bf.toString().trim(), new PdfPage(table.getStartPageNumber(), table.getEndPageNumber()));
            pdfTable.setValues(table.getValues());
//            pdfTable.setHeaders(table.getHeaders());
//            pdfTable.setKeys(table.getKeys());
            pdfTables.add(pdfTable);
        }
        return pdfTables;
    }

    // 3
    private File tables2Sheet(List<PdfTable> tables, String name) throws IOException {
        if (tables == null || tables.isEmpty()) return null;
        XSSFWorkbook workbook = new XSSFWorkbook();
        for (int i = 0; i < tables.size(); i++) {
            PdfTable tb = tables.get(i);
            Object obj = tb.getValues();
            if (!(obj instanceof List)) {
                continue;
            }
            System.out.println(">>>> process table: " + i);
            List<String[]> values = (List<String[]>) obj;
//            XSSFSheet sheet = workbook.createSheet(name + "_" + i + "_" + tb.getTitle());
            XSSFSheet sheet = workbook.createSheet(name + "_" + i);
            // values:: mxn == jxk
            for (int j = 0; j < values.size(); j++) {
                String[] vs = values.get(j);
                Row lineRow = sheet.createRow(j);
                for (int k = 0; k < vs.length; k++) {
//                    System.out.print(vs[k]);
//                    System.out.print("\t\t");
                    lineRow.createCell(k).setCellValue(vs[k]);
                }
//                System.out.println();
            }
            // Resize all columns to fit the content size
            int count = 0;
            if (!values.isEmpty()) count = values.get(0).length;
            for (int l = 0; l < count; l++) {
                sheet.autoSizeColumn(i);
            }
        }
        return save2File(workbook, name);
    }

    private File save2File(XSSFWorkbook book, String name) throws IOException {
        String filePath = storageLocation + "/xlsx";
        String fileName = filePath + "/" + name + new Date().getTime() + ".xlsx";
        File path = new File(filePath);
        if (!path.exists()) {
            path.mkdir();
        }
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream outputStream = new FileOutputStream(file);
        book.write(outputStream);
        outputStream.close();
        book.close();
        return file;
    }

}
