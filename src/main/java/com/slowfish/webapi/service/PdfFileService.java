package com.slowfish.webapi.service;

import com.slowfish.webapi.model.PdfInfo;

import java.util.List;

/**
 * 把今天最好的表现当作明天最新的起点．．～
 * いま 最高の表現 として 明日最新の始発．．～
 * Today the best performance  as tomorrow newest starter!
 * Created by IntelliJ IDEA.
 **/


public interface PdfFileService {

    /**
     * 查所有
     *
     * @return
     */
    List<PdfInfo> findAll();

    /**
     * 根据id查
     *
     * @param id
     * @return
     */
    PdfInfo findById(Long id);

    /**
     * 添加
     *
     * @param PdfInfo
     * @return
     */
    PdfInfo add(PdfInfo PdfInfo);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 更新
     *
     * @param PdfInfo
     * @return
     */
    PdfInfo update(PdfInfo PdfInfo);

}
