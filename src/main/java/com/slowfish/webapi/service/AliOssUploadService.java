package com.slowfish.webapi.service;

import com.aliyun.oss.OSSClient;

import java.io.File;

public interface AliOssUploadService {
    OSSClient getOSSClient();

    String createBucketName(OSSClient ossClient, String bucketName);

    String uploadObject2OSS(OSSClient ossClient, File file, String bucketName, String folder, String fileName);
}
