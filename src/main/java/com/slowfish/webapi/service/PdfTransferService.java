package com.slowfish.webapi.service;

import com.slowfish.webapi.model.PdfInfo;
import com.slowfish.webapi.model.PdfScreenShot;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface PdfTransferService {

    PdfInfo transfer(MultipartFile pdfFile, Long id, String code, String name) throws IOException;

    List<PdfScreenShot> getScreenShots(File pdfFile, String folderName) throws IOException;
}
