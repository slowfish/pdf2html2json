//package com.slowfish.webapi.controller;
//
//import com.slowfish.core.ContentConverter;
//import com.slowfish.core.TableDetector;
//import com.slowfish.core.content.Content;
//import com.slowfish.core.impl.ContentConverterImpl;
//import com.slowfish.core.impl.TableConverterImpl;
//import com.slowfish.core.impl.TableDetectorImpl;
//import com.slowfish.core.tables.RectTable;
//import com.slowfish.core.tables.Table;
//import com.slowfish.webapi.model.*;
//import com.slowfish.webapi.service.PdfFileService;
//import io.swagger.annotations.Api;
//import org.apache.commons.io.FileUtils;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * 把今天最好的表现当作明天最新的起点．．～
// * いま 最高の表現 として 明日最新の始発．．～
// * Today the best performance  as tomorrow newest starter!
// * Created by IntelliJ IDEA.
// **/
//
//@RestController
//@RequestMapping("/api/v1/pdf-transfer")
//@Api("測試")
//public class PdfFileController2 {
//
//    private final PdfFileService service;
//
//    @Autowired
//    public PdfFileController2(PdfFileService service) {
//        this.service = service;
//    }
//
//    @PostMapping
//    public PdfInfo transfer(@RequestParam(required = true) String code,
//                            @RequestParam(required = true) String name,
//                            @RequestParam(required = true) String[] screenShots,
//                            MultipartFile file) throws IOException {
//        File file2 = new File(file.getOriginalFilename());
//        FileUtils.writeByteArrayToFile(file2, file.getBytes());
//        Document document = Jsoup.parse(file2, "utf-8");
//        Element pages = document.body().getElementById("page-container");
//
//        PdfInfo pdfInfo = new PdfInfo();
//        pdfInfo.setBasicInfo(new PdfBasicInfo(code, name, new Date()));
//
//        // table
//        TableDetector detector = new TableDetectorImpl();
//        List<RectTable> rectTables = detector.detect(pages.children());
//        List<Table> tables = new TableConverterImpl().process(rectTables);
//        List<PdfTable> pdfTables = new ArrayList<>();
//        for (Table table : tables) {
//            String[] names = table.getName();
//            StringBuffer bf = new StringBuffer();
//            if (null != names) {
//                bf.append(names[0]);
//                if (names.length > 1)
//                    bf.append("  " + names[1]);
//            }
//            PdfTable pdfTable = new PdfTable(bf.toString().trim(), new PdfPage(table.getStartPageNumber(), table.getEndPageNumber()));
//            pdfTable.setValues(table.getJsonTable());
//            pdfTable.setHeaders(table.getHeaders());
//            pdfTable.setKeys(table.getKeys());
//            pdfTables.add(pdfTable);
//        }
//        // content
//        ContentConverter contentConverter = new ContentConverterImpl();
//        List<Content> contents = contentConverter.convert(pages.children());
//        List<PdfContent> pdfContents = new ArrayList<>();
//        for (Content content : contents) {
//            PdfContent pdfContent = new PdfContent(content.getContentText(), new PdfPage(content.getStartPageNumber(), content.getEndPageNumber()));
//            pdfContents.add(pdfContent);
//        }
//        // screen shots
//        List<PdfScreenShot> screenShotsList = new ArrayList<>();
//        if (null != screenShots && screenShots.length != 0)
//            for (int i = 0; i < screenShots.length; i++) {
//                screenShotsList.add(new PdfScreenShot(i + 1, screenShots[i]));
//            }
//        pdfInfo.setTables(pdfTables);
//        pdfInfo.setContents(pdfContents);
//        pdfInfo.setScreenShots(screenShotsList);
//        pdfInfo.setId(Long.parseLong(code + "" + System.currentTimeMillis() / 1523 + "" + (int) (1000 * Math.random())));
//        return service.add(pdfInfo);
//    }
//}
