package com.slowfish.webapi.controller;

import com.slowfish.webapi.model.PdfInfo;
import com.slowfish.webapi.service.PdfTransferService;
import com.slowfish.webapi.service.PdfTransferSheetService;
import com.slowfish.webapi.service.impl.PdfTransferSheetServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 把今天最好的表现当作明天最新的起点．．～
 * いま 最高の表現 として 明日最新の始発．．～
 * Today the best performance  as tomorrow newest starter!
 * Created by IntelliJ IDEA.
 **/

@RestController
@RequestMapping("/api/v1/pdf-transfer")
@Api("測試")
public class PdfFileController {

    @Autowired
    private PdfTransferService pdfTransferService;

    @Autowired
    private PdfTransferSheetService pdfTransferSheetService;

    @PostMapping
    public PdfInfo transfer(@RequestParam(required = true) String code,
                            @RequestParam(required = true) String name,
                            MultipartFile file) throws IOException {
        Long id = (Long.parseLong(code + "" + System.currentTimeMillis() / 1523 + "" + (int) (1000 * Math.random())));
        return pdfTransferService.transfer(file, id, code, name);
    }

    @PostMapping("/export-xls")
    public File sheet(@RequestParam(required = true) String name,
                      MultipartFile file) throws IOException, InterruptedException {
        return pdfTransferSheetService.transfer(file, name);
    }
}
