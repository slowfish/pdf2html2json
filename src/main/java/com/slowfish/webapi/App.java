package com.slowfish.webapi;


import com.mongodb.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * 把今天最好的表现当作明天最新的起点．．～
 * いま 最高の表現 として 明日最新の始発．．～
 * Today the best performance  as tomorrow newest starter!
 * Created by IntelliJ IDEA.
 **/
@Configuration
@ComponentScan("com.slowfish.webapi")
@EntityScan("com.slowfish.webapi.model")
@RestController
@EnableSwagger2
//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableAutoConfiguration
@EnableMongoRepositories
@SpringBootApplication(scanBasePackages = "com.slowfish.webapi")
@PropertySource("classpath:application.properties")
public class App extends SpringBootServletInitializer {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiIgnore()
    @ApiOperation(value = "重定向到api首页")
    public ModelAndView index() {
        return new ModelAndView("redirect:/swagger-ui.html");
    }

    @Autowired
    private MongoDbFactory factory;
    @Autowired
    private MongoMappingContext context;

    @Bean
    public MappingMongoConverter mappingMongoConverter() throws Exception {
        MappingMongoConverter converter = new MappingMongoConverter(factory, context);
//        MongoClientURI uri = new MongoClientURI("mongodb://neo:123456@dds-m5e5d8264bab0fb41671-pub.mongodb.rds.aliyuncs.com:3717,dds-m5e5d8264bab0fb42701-pub.mongodb.rds.aliyuncs.com:3717/aimidas?replicaSet=mgset-5554137");
//        MappingMongoConverter converter = new MappingMongoConverter(new DefaultDbRefResolver(new SimpleMongoDbFactory(uri)), new MongoMappingContext());
//        converter.setMapKeyDotReplacement("\\+");
        converter.setMapKeyDotReplacement("_");
        return converter;
    }

//    @Bean
//    public Mongo mongo() {
//        String host = "dds-m5e5d8264bab0fb41671-pub.mongodb.rds.aliyuncs.com:3717";
//        String username = "neo";
//        String name = "aimidas";
//        String password = "123456";
//        ServerAddress serverAddress = new ServerAddress(host);
//        List<MongoCredential> credentials = new ArrayList<>();
//        credentials.add(MongoCredential.createScramSha1Credential(
//                username,
//                name,
//                password.toCharArray()
//        ));
//        MongoClientOptions options = new MongoClientOptions.Builder()
//                .build();
//        Mongo mongo = new MongoClient(serverAddress, credentials, options);
//        return mongo;
//    }

//    @Bean
//    public MongoTemplate mongoTemplate() {
//        return new MongoTemplate(mongo(), "aimidas");
//    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.slowfish.webapi"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Pdf -> MongoDB")
                .description("Pdf -> MongoDB")
                .termsOfServiceUrl("https://slowfish.com/")
                .contact("小亦")
                .version("1.0")
                .build();
    }
}
