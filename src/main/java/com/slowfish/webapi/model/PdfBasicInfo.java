package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;


@Data
@ToString(callSuper = false)
//@NoArgsConstructor
//@AllArgsConstructor
public class PdfBasicInfo {

    @ApiModelProperty(value = "")
    private String name;

    private String code;

    private Date updateTime;

    public PdfBasicInfo(String code, String name, Date updateTime) {
        this.code = code;
        this.name = name;
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
