package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;


@Data
@ToString(callSuper = false)
//@NoArgsConstructor
//@AllArgsConstructor
public class PdfPage {

    @ApiModelProperty(value = "")
    private Long startPage;

    private Long endPage;

    public PdfPage(Long startPage, Long endPage) {
        this.startPage = startPage;
        this.endPage = endPage;
    }

    public Long getStartPage() {
        return startPage;
    }

    public void setStartPage(Long startPage) {
        this.startPage = startPage;
    }

    public Long getEndPage() {
        return endPage;
    }

    public void setEndPage(Long endPage) {
        this.endPage = endPage;
    }
}
