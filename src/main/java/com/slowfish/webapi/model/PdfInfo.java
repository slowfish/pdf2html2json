package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "PDFInfo")
@Data
@ToString(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class PdfInfo {

    @Id
    private Long id;

    @ApiModelProperty(value = "")
    private PdfBasicInfo basicInfo;

    private List<PdfScreenShot> screenShots;

    private List<PdfContent> contents;

    private List<PdfTable> tables;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PdfBasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(PdfBasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public List<PdfContent> getContents() {
        return contents;
    }

    public void setContents(List<PdfContent> contents) {
        this.contents = contents;
    }

    public List<PdfTable> getTables() {
        return tables;
    }

    public void setTables(List<PdfTable> tables) {
        this.tables = tables;
    }

    public List<PdfScreenShot> getScreenShots() {
        return screenShots;
    }

    public void setScreenShots(List<PdfScreenShot> screenShots) {
        this.screenShots = screenShots;
    }
}
