package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@Data
@ToString(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class PdfTable {

    @ApiModelProperty(value = "")
    private String title;

    private PdfPage page;

    private List<Object> keys;

    private List<Object> headers;

    private Object values;

    public PdfTable(String title, PdfPage page) {
        this.title = title;
        this.page = page;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PdfPage getPage() {
        return page;
    }

    public void setPage(PdfPage page) {
        this.page = page;
    }

    public List<Object> getKeys() {
        return keys;
    }

    public void setKeys(List<Object> keys) {
        this.keys = keys;
    }

    public List<Object> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Object> headers) {
        this.headers = headers;
    }

    public Object getValues() {
        return values;
    }

    public void setValues(Object values) {
        this.values = values;
    }
}
