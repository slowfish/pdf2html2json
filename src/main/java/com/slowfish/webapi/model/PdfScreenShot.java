package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;


@Data
@ToString(callSuper = false)
//@NoArgsConstructor
//@AllArgsConstructor
public class PdfScreenShot {

    @ApiModelProperty(value = "")
    private Integer pageNumer;

    private String url;

    public PdfScreenShot(Integer pageNumer, String url) {
        this.pageNumer = pageNumer;
        this.url = url;
    }

    public Integer getPageNumer() {
        return pageNumer;
    }

    public void setPageNumer(Integer pageNumer) {
        this.pageNumer = pageNumer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
