package com.slowfish.webapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
//@ToString(callSuper = false)
//@NoArgsConstructor
//@AllArgsConstructor
public class PdfContent {

    @ApiModelProperty(value = "")
    private String text;

    private PdfPage page;

    public PdfContent(String text, PdfPage page) {
        this.text = text;
        this.page = page;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public PdfPage getPage() {
        return page;
    }

    public void setPage(PdfPage page) {
        this.page = page;
    }
}
