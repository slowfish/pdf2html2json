package com.slowfish;

import com.slowfish.core.impl.TableConverterImpl;
import com.slowfish.core.tables.RectTable;
import com.slowfish.core.TableDetector;
import com.slowfish.core.impl.TableDetectorImpl;
import com.slowfish.core.tables.Table;
import com.slowfish.util.JSONPrinter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException {
        JSONPrinter printer = new JSONPrinter();
        File file = new File("src/main/resources/1204378896.html");
        Document document = Jsoup.parse(file, "utf-8");
        Element pages = document.body().getElementById("page-container");
        TableDetector detector = new TableDetectorImpl();
        List<RectTable> rectTables = detector.detect(pages.children());
        List<Table> tables = new TableConverterImpl().process(rectTables);
//        int i = 0;
//        for (RectTable rectTable : rectTables) {
//            String[] names = rectTable.getName();
//            if (null != names)
//                System.out.println(names[0]);
//            System.out.println(rectTable.getStartPageNumber());
//            System.out.println(rectTable.getEndPageNumber());
//
//            if (i++ == 40) break;
//        }
        for (Table table : tables) {
            System.out.print(">> 表格可能名称：\t");
            String[] names = table.getName();
            if (null != names) {
                System.out.print(names[0]);
                if (names.length > 1)
                    System.out.print("\t|\t" + names[1]);
            }
            System.out.println("\n"); // 换两行
            System.out.println(printer.formatJson(table.getJsonTable().toJSONString()));
            System.out.println("\n"); // 换三行
        }
    }
}

