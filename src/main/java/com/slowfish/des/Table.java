package com.slowfish.des;

import com.slowfish.core.exception.TableOneLineException;

import java.util.List;
import java.util.Map;

@Deprecated
public interface Table {

    void setTableId(Long id);

    void setTableName(String name);

    void setTableDescription(String description);

    Long getTableId();

    String getTableName();

    String getTableDescription();

    /**
     * 将新的一组数据添加进来形成新的一行，必须一行行添加，会验证是否在同一行，格式：<div class="c x1 yc8 w25 h2d"></div> 为一个格子
     */
    Table append(String... cells) throws TableOneLineException;

    /**
     * 将新数据添加进来形成新的行，多行添加，会验证是否在同一行，格式：<div class="c x1 yc8 w25 h2d"></div> 为一个格子
     */
    Table append(List<String[]> lines) throws TableOneLineException;

    /**
     * 列出现有的原数据，未经修改的
     *
     * @return
     */
    List<String[]> listSrcData();

    /**
     * 列出原始数据
     *
     * @param startLine
     * @param endLine
     * @return
     */
    List<String[]> listSrcData(int startLine, int endLine);

    /**
     * 验证是否在同一行
     *
     * @param cells
     * @return
     */
    boolean onSameLine(String[] cells);

    /**
     * 将多个 cells 合并成多行
     *
     * @param cells
     * @return 第 0 个为原始 html 数据，第 1 个为纯文本数据
     */
    List<String[]>[] split(String... cells);


    String getPlainText(String cell);

    /**
     * 处理并返回最终 json 结果
     *
     * @return String: key, Object: value
     */
    Map<String, Object> process2JSON();

    /**
     * 获取 keys
     *
     * @return
     */
    List<String> getKeys();

    /**
     * 获取表头
     *
     * @return
     */
    List<String> getHeaders();

    /**
     * 获取某一个 key 的值
     *
     * @param key
     * @return
     */
    Object getValues(String key);

    /**
     * 获取所有值
     *
     * @return
     */
    Map<String, Object> getValues();

    /**
     * 打印转化后的 JSON 数据
     *
     * @return
     */
    String getTable2JSON();

    /**
     * 打印纯文本的 Table
     *
     * @return
     */
    String getTablePlain();

    /**
     * 打印原始 Table
     *
     * @return
     */
    String getTableHtml();

}
