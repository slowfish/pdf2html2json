package com.slowfish.des.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.slowfish.des.Table;
import com.slowfish.core.exception.TableOneLineException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;

/**
 * 先处理简单表格：表头只有一行、key 只有一列的
 */

@Deprecated
public class TableImpl implements Table {

    private Long id;
    private String name;
    private String description;
    private List<String[]> srcData = new ArrayList<>();
    private List<String[]> middleData = new ArrayList<>(); // 去掉 html 标签，保留纯文本的数据
    private Map<String, Object> jsonData = new HashMap<>();
    private List<String> headers = new ArrayList<>();
    private List<String> keys = new ArrayList<>();
    private Map<String, Object> values = new HashMap<>();

    public TableImpl() {
//        this.srcData = new ArrayList<String[]>();
//        this.middleData = new ArrayList<String[]>();
//        this.jsonData = new HashMap<>();
    }

    public TableImpl(String srcHtml) {
//        this.srcData = new ArrayList<String[]>();
//        this.middleData = new ArrayList<String[]>();
//        this.jsonData = new HashMap<>();

        List[] lists = split(srcHtml);
        this.srcData.addAll(lists[0]);
        this.middleData.addAll(lists[1]);
        process2JSON(); // 默认执行转化 json 操作
    }

    public TableImpl(String[] cells) throws TableOneLineException {
//        this.srcData = new ArrayList<String[]>();
//        this.middleData = new ArrayList<String[]>();
//        this.jsonData = new HashMap<>();
        // 验证是否在同一行 #弃用
//        if (null == lines || lines.isEmpty()) return;
//        for (String[] cells : lines) {
//            if (!onSameLine(cells)) {
//                throw new TableOneLineException("", cells);
//            }
//        }
        List[] lists = split(cells);
        this.srcData.addAll(lists[0]);
        this.middleData.addAll(lists[1]);
    }

    public void setTableId(Long id) {
        this.id = id;
    }

    public void setTableName(String name) {
        this.name = name;
    }

    public void setTableDescription(String description) {
        this.description = description;
    }

    public Long getTableId() {
        return this.id;
    }

    public String getTableName() {
        return this.name;
    }

    public String getTableDescription() {
        return this.description;
    }

    public Table append(String... cells) throws TableOneLineException {
        if (onSameLine(cells))
            this.srcData.add(cells);
        return this;
    }

    public Table append(List<String[]> lines) throws TableOneLineException {
        for (String[] cells : lines) {
            if (!onSameLine(cells)) {
                throw new TableOneLineException("", cells);
            }
        }
        return this;
    }

    public List<String[]> listSrcData() {
        List<String[]> newList = new ArrayList<>();
        newList.addAll(this.srcData);
        return newList;
    }

    public List<String[]> listSrcData(int startLine, int endLine) {
        List<String[]> newList = new ArrayList<>();
        if (startLine > endLine) return newList;
        if (startLine > this.srcData.size()) return newList;
//        if (endLine > this.srcData.size()) endLine = this.srcData.size();
        for (int i = startLine; i < endLine && i < this.srcData.size(); i++) {
            newList.add(this.srcData.get(i));
        }
        return newList;
    }

    // <div class="c x1 yc8 w25 h2d"></div>
    public boolean onSameLine(String[] cells) {
        StringBuffer bf = new StringBuffer();
        for (String cell : cells) {
            if (null == cell) continue;
            bf.append(cell);
        }
        String currentClassName = null;
        boolean flag = false;
        Document document = Jsoup.parse(bf.toString());
        Elements els = document.select("div");
        for (Element el : els) {
            if (null == el) continue;
            Set<String> classNames = el.classNames();
            if (classNames.size() != 5) continue; // class 必须是 5 个值
            Object[] cla = classNames.toArray();
            if (null == currentClassName)
                currentClassName = cla[2].toString();
            else
                flag = currentClassName.equals(cla[2].toString());
        }
        return flag;
    }

    @Override
    public List<String[]>[] split(String... cells) {
        StringBuffer bf = new StringBuffer();
        for (String cell : cells) {
            if (null == cell) continue;
            bf.append(cell);
        }
        return split(bf.toString());
    }

    public List<String[]>[] split(String srcData) {
        if (null == srcData || srcData.isEmpty())
            return new List[]{new ArrayList(), new ArrayList()};
        List<String[]> linesHtml = new ArrayList<>();
        List<String[]> linesPlain = new ArrayList<>();
        List<String> currentLineHtml = null;
        List<String> currentLinePlain = null;
        String currentClassName = null;
        Document document = Jsoup.parse(srcData);
        Elements els = document.body().children();
        for (Element el : els) {
            if (null == el) continue;
            Set<String> classNames = el.classNames();
            if (classNames.size() != 5) continue; // class 必须是 5 个值
            Object[] cla = classNames.toArray();
            if (!"c".equals(cla[0])) continue; // class 第一个值必须是 c （表示 cell，非 text）
            if (null == currentClassName) { // 说明这是第一行
                // html
                currentLineHtml = new ArrayList<>();
                currentLineHtml.add(el.html());
                // plain
                currentLinePlain = new ArrayList<>();
                currentLinePlain.add(getPlainText(el));
                //
                currentClassName = cla[2].toString();
            } else if (currentClassName.equals(cla[2].toString())) { // 如果和上一行一致
                currentLineHtml.add(el.html());
                currentLinePlain.add(getPlainText(el));
            } else { // 新的行了
                // html
                linesHtml.add(currentLineHtml.toArray(new String[currentLineHtml.size()]));
                currentLineHtml = new ArrayList<>();
                currentLineHtml.add(el.html());
                // plain
                linesPlain.add(currentLinePlain.toArray(new String[currentLinePlain.size()]));
                currentLinePlain = new ArrayList<>();
                currentLinePlain.add(getPlainText(el));
                //
                currentClassName = cla[2].toString();
            }
        }
        return new List[]{linesHtml, linesPlain};
    }

    private String getPlainText(Element element) {
        return element.text();
    }

    @Override
    public String getPlainText(String cell) {
        Element element = new Element(cell);
        return element.text();
    }

    // 将 src/middle 的数据转化为 json, return values
    @Override
    public Map<String, Object> process2JSON() {
//        if (this.srcData.isEmpty()) return new HashMap<>();
        // 从 middle 转，成立条件：key 只有 1 列，header 只有 1 行
        if (this.middleData.isEmpty()) return new HashMap<>();
        keys.clear();
        headers.clear();
        values.clear();
        // keys
        for (int i = 1; i < this.middleData.size(); i++) {
            String[] currentLine = this.middleData.get(i);
            keys.add(currentLine[0]);
        }
        // headers
        String[] headerLine = this.middleData.get(0);
        for (int i = 0; i < headerLine.length; i++) {
            headers.add(headerLine[i]);
        }
        // values
//        int row = this.middleData.size();
//        int columns = this.middleData.get(0).length;
        for (int i = 1; i < this.middleData.size(); i++) {
            String[] currentLine = this.middleData.get(i);
            Map<String, String> map = new HashMap();
            for (int j = 1; j < currentLine.length; j++) {
                // 从第 1 行第 1 列走
                map.put(headers.get(j), currentLine[j]); // {'主要变动原因': '业务增长'}
            }
            values.put(keys.get(i - 1), map); // {'总资产': {'主要变动原因': '业务增长'}}
        }
        return values;
    }

    public List<String> getKeys() {
        return this.keys;
    }

    public List<String> getHeaders() {
        return this.headers;
    }

    public Object getValues(String key) {
        if (null == this.values) return null;
        return this.values.get(key);
    }

    public Map<String, Object> getValues() {
        return this.values;
    }

    public String getTable2JSON() {
        JSONObject obj = (JSONObject)JSON.toJSON(this.values);
        String json = obj.toJSONString();
//        System.out.println(json);
        return json;
    }

    @Override
    public String getTablePlain() {
        StringBuffer bf = new StringBuffer();
        for (int i = 0; i < this.middleData.size(); i++) {
            String[] currentLine = this.middleData.get(i);
            for (int j = 0; j < currentLine.length; j++) {
                bf.append("| ").append(currentLine[j]).append("\t\t\t");
            }
            bf.append("\n");
        }
        String tableStr = bf.toString();
//        System.out.println(tableStr);
        return tableStr;
    }

    @Override
    public String getTableHtml() {
        StringBuffer bf = new StringBuffer();
        for (int i = 0; i < this.srcData.size(); i++) {
            String[] currentLine = this.srcData.get(i);
            for (int j = 0; j < currentLine.length; j++) {
                // 此处禁止多行打印
                bf.append(currentLine[j].replace("\n", " ")).append("\t");
            }
            bf.append("\n");
        }
        String tableStr = bf.toString();
//        System.out.println(tableStr);
        return tableStr;
    }
}
