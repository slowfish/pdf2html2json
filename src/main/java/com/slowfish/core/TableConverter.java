package com.slowfish.core;

import com.slowfish.core.tables.RectTable;
import com.slowfish.core.tables.Table;

import java.util.List;

/**
 * 将 Rect 的原始数据 table 转换为 最终数据（支持 json）
 */
public interface TableConverter {

    List<Table> process(List<RectTable> tables);
}
