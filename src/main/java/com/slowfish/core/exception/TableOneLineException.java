package com.slowfish.core.exception;

public class TableOneLineException extends Exception {

    public TableOneLineException(String cell, String... cells) {
//        super(new StringBuffer().append(cells).toString());
        System.err.println(printMessage(cell, cells));
    }

    String printMessage(String cell, String... cells) {
        StringBuffer bf = new StringBuffer();
        bf.append("存在数据不在同一行：\n")
                .append("\t\t").append(cell).append("\n")
                .append("原始数据：");
        if (null != cells && cells.length != 0)
            for (String cel : cells)
                bf.append("\t\t").append(cel).append("\n");
        else
            bf.append("\t\t原始数据数据为空");
        return bf.toString();
    }
}
