package com.slowfish.core.tables.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.slowfish.core.tables.RectTable;
import com.slowfish.core.tables.Table;
import org.jsoup.nodes.Element;

import java.util.*;

/**
 * 仅支持：一行表头、一列 key 值
 */
public class SheetTable implements Table {
    private String[] title;
    private long startPage;
    private long endPage;
    private List<Element[]> srcData = new ArrayList<>();
    private List<String[]> middleData = new ArrayList<>(); // 去掉 html 标签，保留纯文本的数据
    // data
//    private List<String> headers = new ArrayList<>();
//    private List<String> keys = new ArrayList<>();
    private List<String[]> values = new ArrayList<>();


    /**
     * 需要手动设定 page 信息
     *
     * @param cells
     */
    public SheetTable(List<Element> cells) {
        process(cells);
    }

    public SheetTable(RectTable rectTable) {
        this.setName(rectTable.getName());
        this.setStartPageNumber(rectTable.getStartPageNumber());
        this.setEndPageNumber(rectTable.getEndPageNumber());
        this.process(rectTable.getHtml());
    }

    @Override
    public List<String[]>[] process(List<Element> cells) {
        List[] lists = split(cells);
        this.srcData.addAll(lists[0]);
        this.middleData.addAll(lists[1]);
        return new List[]{srcData, middleData};
    }

    @Override
    public List<String[]> getPlainTable() {
        return this.middleData;
    }

    @Override
    public List<Element[]> getHtmlTable() {
        return this.srcData;
    }

    @Override
    public List<Object> getHeaders() {
        return new ArrayList<>();
    }

    @Override
    public List<Object> getKeys() {
        return new ArrayList<>();
    }

    @Override
    public Object getValues() {
        return process2JSON();
    }

    @Override
    public JSONObject getJsonTable() {
        return (JSONObject) JSON.toJSON(this.process2JSON());
    }

    private List<String[]> process2JSON() {
        // 从 middle 转，成立条件：key 只有 1 列，header 只有 1 行，矩形 mxn
        if (this.middleData.isEmpty()) return new ArrayList<>();
        // values
//        int row = this.middleData.size();
//        int columns = this.middleData.get(0).length;
        for (int i = 0; i < this.middleData.size(); i++) {
            String[] currentLine = this.middleData.get(i);
            values.add(currentLine);
        }
        return values;
    }

    // 重组成表
    public List<String[]>[] split(List<Element> elements) {
        if (null == elements || elements.isEmpty())
            return new List[]{new ArrayList(), new ArrayList()};
        List<Element[]> linesHtml = new ArrayList<>();
        List<String[]> linesPlain = new ArrayList<>();
        List<Element> currentLineHtml = null;
        List<String> currentLinePlain = null;
        String currentClassName = null;
        for (Element el : elements) {
            if (null == el) continue;
            Set<String> classNames = el.classNames();
            if (classNames.size() != 5) continue; // class 必须是 5 个值
            Object[] cla = classNames.toArray();
            if (!"c".equals(cla[0])) continue; // class 第一个值必须是 c （表示 cell，非 text）
            if (null == currentClassName) { // 说明这是第一行
                // html
                currentLineHtml = new ArrayList<>();
                currentLineHtml.add(el);
                // plain
                currentLinePlain = new ArrayList<>();
                currentLinePlain.add(el.text());
                //
                currentClassName = cla[2].toString();
            } else if (currentClassName.equals(cla[2].toString())) { // 如果和上一行一致
                currentLineHtml.add(el);
                currentLinePlain.add(el.text());
            } else { // 新的行了
                // html
                linesHtml.add(currentLineHtml.toArray(new Element[currentLineHtml.size()]));
                currentLineHtml = new ArrayList<>();
                currentLineHtml.add(el);
                // plain
                linesPlain.add(currentLinePlain.toArray(new String[currentLinePlain.size()]));
                currentLinePlain = new ArrayList<>();
                currentLinePlain.add(el.text());
                //
                currentClassName = cla[2].toString();
            }
        }
        return new List[]{linesHtml, linesPlain};
    }

    @Override
    public void setName(String[] title) {
        this.title = title;
    }

    @Override
    public String[] getName() {
        return this.title;
    }

    @Override
    public void setStartPageNumber(long pageNumber) {
        this.startPage = pageNumber;
    }

    @Override
    public Long getStartPageNumber() {
        return this.startPage;
    }

    @Override
    public void setEndPageNumber(long pageNumber) {
        this.endPage = pageNumber;
    }

    @Override
    public Long getEndPageNumber() {
        return this.endPage;
    }

}
