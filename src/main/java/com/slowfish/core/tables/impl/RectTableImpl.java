package com.slowfish.core.tables.impl;

import com.slowfish.core.tables.RectTable;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RectTableImpl implements RectTable {

    private String[] title;
    private long startPage;
    private long endPage;
    private List<Element> cells = new ArrayList<>();

    @Override
    public void setName(String[] title) {
        this.title = title;
    }

    @Override
    public String[] getName() {
        return this.title;
    }

    @Override
    public void setStartPageNumber(long pageNumber) {
        this.startPage = pageNumber;
    }

    @Override
    public Long getStartPageNumber() {
        return this.startPage;
    }

    @Override
    public void setEndPageNumber(long pageNumber) {
        this.endPage = pageNumber;
    }

    @Override
    public Long getEndPageNumber() {
        return this.endPage;
    }

    @Override
    public List<Element> append(Element cell) {
        if (null == this.cells) this.cells = new ArrayList<>();
        this.cells.add(cell);
        return this.cells;
    }

    @Override
    public List<Element> getHtml() {
        return this.cells;
    }

    @Override
    public TYPE getType() {
        // 如果是标准的 n*m 就暂时判断为 simple
        List<Integer[]> linesHint = new ArrayList<>();
        List<Integer> currentLineHint = null;
        String currentClassName = null;
        for (Element el : this.cells) {
            if (null == el) continue;
            Set<String> classNames = el.classNames();
            if (classNames.size() != 5) continue; // class 必须是 5 个值
            Object[] cla = classNames.toArray();
            if (!"c".equals(cla[0])) continue; // class 第一个值必须是 c （表示 cell，非 text）
            if (null == currentClassName) { // 说明这是第一行
                // plain
                currentLineHint = new ArrayList<>();
                currentLineHint.add(0000000000); // 标记这里有东西而已
                //
                currentClassName = cla[2].toString();
            } else if (currentClassName.equals(cla[2].toString())) { // 如果和上一行一致
                currentLineHint.add(0000000000); // 标记这里有东西而已
            } else { // 新的行了
                // plain
                linesHint.add(currentLineHint.toArray(new Integer[currentLineHint.size()]));
                currentLineHint = new ArrayList<>();
                currentLineHint.add(0000000000); // 标记这里有东西而已
                //
                currentClassName = cla[2].toString();
            }
        }
        if (linesHint.isEmpty()) return TYPE.other;
        boolean flag = true;
        Integer currentWidth = linesHint.get(0).length;
        for (int i = 1; i < linesHint.size(); i++) {
            Integer[] widthHint = linesHint.get(i);
            if (null == widthHint) continue;
            int width = widthHint.length;
            if (!currentWidth.equals(width)) {
                // 不是 m*n 的 simple 类型
                flag = false;
                break;
            }
            currentWidth = width;
        }
//        return flag ? TYPE.simple : TYPE.other;
        return flag ? TYPE.sheet : TYPE.other; // updated at 2018-08-14 export sheets
    }
}
