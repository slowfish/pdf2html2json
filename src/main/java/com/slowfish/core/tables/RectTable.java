package com.slowfish.core.tables;

import org.jsoup.nodes.Element;

import java.util.List;

/**
 * 保留 html 格式的 table 信息
 */
public interface RectTable {

    public enum TYPE {
        simple,
        other,
        sheet, // can be exported to xls
    }

    void setName(String[] title);

    String[] getName();

    void setStartPageNumber(long pageNumber);

    Long getStartPageNumber();

    void setEndPageNumber(long pageNumber);

    Long getEndPageNumber();

//    List<Element> convert(List<String> html);

    List<Element> append(Element cell);

    List<Element> getHtml(); // 输出当前存储的结果

    TYPE getType();
}
