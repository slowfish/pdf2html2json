package com.slowfish.core.tables;

import com.alibaba.fastjson.JSONObject;
import org.jsoup.nodes.Element;

import java.util.List;

public interface Table {

    void setName(String[] title);

    String[] getName();

    void setStartPageNumber(long pageNumber);

    Long getStartPageNumber();

    void setEndPageNumber(long pageNumber);

    Long getEndPageNumber();

    List<String[]>[] process(List<Element> cells);

    List<String[]> getPlainTable();

    List<Element[]> getHtmlTable();

    List<Object> getHeaders();

    List<Object> getKeys();

    // updated at 2018-08-14 for sheet export
    Object getValues();

    JSONObject getJsonTable();
}
