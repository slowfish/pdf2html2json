package com.slowfish.core;

import com.slowfish.core.content.Content;
import org.jsoup.nodes.Element;

import java.util.List;

/**
 * 传入整个 pdf 页面，转出段落，段落按表格分开，即：两个表之间的文字算一个段落
 */
public interface ContentConverter {
    List<Content> convert(List<Element> elementLines);
}
