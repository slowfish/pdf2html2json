package com.slowfish.core.impl;

import com.slowfish.core.ContentConverter;
import com.slowfish.core.content.Content;
import com.slowfish.core.content.impl.ContentImpl;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class ContentConverterImpl implements ContentConverter {
    // <div id="pf5" class="pf w0 h0" data-page-no="5"></div>
    @Override
    public List<Content> convert(List<Element> elementLines) {
        if (null == elementLines || elementLines.isEmpty()) return new ArrayList<>();
        List<Content> contents = new ArrayList<>();
        boolean needCreateNewContent = true;
        Content currentContent = null;
        for (Element element : elementLines) {
            // 获取页码
            long pageNumber = -1;
            String pageNumberStr = element.attr("data-page-no");
            if (null != pageNumberStr)
                pageNumber = Long.parseLong(pageNumberStr, 16);
            // 获取该页 elements
            Element pc = element.child(0);
            Elements children = pc.children();
            int size = children.size();
            for (int i = 3; i < size; i++) { // 第 0 个是 <img>， 1 是页头，2 是页尾
                Element line = children.get(i);
                if (isTextContent(line)) {
                    if (needCreateNewContent) {
                        // 这可能是这段的最后一条数据
                        if (null != currentContent) {
                            currentContent.forceProcess(); // 强制刷新一遍，确保数据更新
                            currentContent.setEndPageNumber(pageNumber);
                        }
                        // 新建一段
                        currentContent = new ContentImpl(null);
                        currentContent.setStartPageNumber(pageNumber);
                        currentContent.append(line);
                        contents.add(currentContent);
                        needCreateNewContent = false;
                    } else {
                        currentContent.append(line);
                    }
                } else {
                    // 这是表格
                    needCreateNewContent = true;
                }
            }
        }
        return contents;
    }

    private boolean isTextContent(Element element) {
        return element.classNames().contains("t");
    }
}
