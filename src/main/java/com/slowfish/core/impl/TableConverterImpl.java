package com.slowfish.core.impl;

import com.slowfish.core.tables.RectTable;
import com.slowfish.core.tables.Table;
import com.slowfish.core.TableConverter;
import com.slowfish.core.tables.impl.SheetTable;
import com.slowfish.core.tables.impl.SimpleTable;

import java.util.ArrayList;
import java.util.List;

public class TableConverterImpl implements TableConverter {
    @Override
    public List<Table> process(List<RectTable> tables) {
        int count = 0;
        int sum = tables.size();
        List<Table> tableList = new ArrayList<>();
        for (RectTable rectTable : tables) {
            if (null == rectTable) continue;
            RectTable.TYPE type = rectTable.getType();
            if (type == RectTable.TYPE.simple) {
                Table table = new SimpleTable(rectTable);
                tableList.add(table);
                count++;
            } else if (type == RectTable.TYPE.sheet) {
                Table table = new SheetTable(rectTable);
                tableList.add(table);
                count++;
            } else if (type == RectTable.TYPE.other) {
                // 其余的 convert 处理，日后可增加更多类型的表，根据 type 字段进行表格类型判断
//                System.err.println("不支持该类型表的转换");
            }
        }
        System.err.println(count);
        System.err.println(sum);
        return tableList;
    }
}
