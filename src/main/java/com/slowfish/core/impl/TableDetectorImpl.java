package com.slowfish.core.impl;

import com.slowfish.core.tables.RectTable;
import com.slowfish.core.TableDetector;
import com.slowfish.core.tables.impl.RectTableImpl;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TableDetectorImpl implements TableDetector {
    // <div id="pf5" class="pf w0 h0" data-page-no="5"></div>
    @Override
    public List<RectTable> detect(List<Element> elements) {
        if (null == elements || elements.isEmpty()) return new ArrayList<>();
        List<RectTable> rectTables = new ArrayList<>();
        boolean needCreateNewTable = false;
        Stack<String> currentText = new Stack<>(); // 存储可能存在的表格名称字段
        boolean isFirstText = false; // 存储表格名称时候，可能有两行表名，用此标记是否连续
        RectTable currentRect = new RectTableImpl();
        for (Element element : elements) {
            // 获取页码
            long pageNumber = -1;
            String pageNumberStr = element.attr("data-page-no");
            if (null != pageNumberStr)
                pageNumber = Long.parseLong(pageNumberStr, 16);
            // 获取所有的 cell
            Element pc = element.child(0);
            Elements children = pc.children();
            int size = children.size();
            // 读取表格数据
            for (int i = 3; i < size; i++) { // 第 0 个是 <img>， 1 是页头，2 是页尾
                Element cell = children.get(i);
                if (isTableCell(cell)) {
                    if (needCreateNewTable) {
                        // 存储以前的表，再新建一个
                        currentRect.setEndPageNumber(pageNumber);
                        rectTables.add(currentRect);
                        // new
                        currentRect = new RectTableImpl();
                        if (currentText.size() == 1) {
                            currentRect.setName(new String[]{currentText.pop()});
                        } else if (currentText.size() > 1) {
                            String name1 = currentText.pop();
                            String name2 = currentText.pop();
                            currentRect.setName(new String[]{name2, name1});
                        } else {
                            currentRect.setName(new String[]{null});
                        }
                        currentRect.setStartPageNumber(pageNumber);
                        needCreateNewTable = false;
                        currentText.clear(); // 清空
                    }
                    // append
                    currentRect.append(cell);
                } else {
                    // 是文本内容，非表格
                    needCreateNewTable = true;
                    currentText.push(cell.text());
                }
            }
        }
        return rectTables;
    }

    private boolean isTableCell(Element element) {
        return element.classNames().contains("c");
    }
}
