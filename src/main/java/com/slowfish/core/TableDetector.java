package com.slowfish.core;

import com.slowfish.core.tables.RectTable;
import org.jsoup.nodes.Element;

import java.util.List;

/**
 * 读取整个 html 文件，输出一组 Table
 */
public interface TableDetector {

    /**
     * @param elements input list <div id="pf5" class="pf w0 h0" data-page-no="5"></div>
     * @return
     */
    List<RectTable> detect(List<Element> elements);
}
