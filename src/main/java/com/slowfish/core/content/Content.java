package com.slowfish.core.content;

import org.jsoup.nodes.Element;

import java.util.List;

public interface Content {

    void setStartPageNumber(long pageNumber);

    Long getStartPageNumber();

    void setEndPageNumber(long pageNumber);

    Long getEndPageNumber();

    String getContentText();

    List<String> getContents();

    List<Element> append(Element line);

    void forceProcess(); // 强制刷新一次，执行
}
