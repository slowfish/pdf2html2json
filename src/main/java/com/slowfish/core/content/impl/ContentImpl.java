package com.slowfish.core.content.impl;

import com.slowfish.core.content.Content;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContentImpl implements Content {

    private long startPage;
    private long endPage;
    private String contentText;
    private List<String> contents;
    private List<Element> srcData;

    public ContentImpl(List<Element> elementLines) {
        if (null != elementLines && !elementLines.isEmpty()) {
            this.srcData = elementLines;
            process(elementLines);
        } else {
            this.srcData = new ArrayList<>();
            contentText = null;
            contents = new ArrayList<>();
        }
    }

    private void process(List<Element> elementLines) {
        if (null == elementLines) return;
        Elements element = new Elements(elementLines);
        this.contentText = element.text();
        this.contents = element.parallelStream().map(Element::text).collect(Collectors.toList());
    }

    @Override
    public void setStartPageNumber(long pageNumber) {
        this.startPage = pageNumber;
    }

    @Override
    public Long getStartPageNumber() {
        return this.startPage;
    }

    @Override
    public void setEndPageNumber(long pageNumber) {
        this.endPage = pageNumber;
    }

    @Override
    public Long getEndPageNumber() {
        return this.endPage;
    }

    @Override
    public String getContentText() {
        if (null == this.contentText)
            process(this.srcData);
        return this.contentText;
    }

    @Override
    public List<String> getContents() {
        if (null == this.contents || this.contents.isEmpty())
            process(this.srcData);
        return this.contents;
    }

    @Override
    public List<Element> append(Element line) {
        if (null == this.contents)
            this.contents = new ArrayList<>();
        if (null == this.srcData)
            this.srcData = new ArrayList<>();
        this.contents.add(line.text());
        this.srcData.add(line);
        return this.srcData;
    }

    @Override
    public void forceProcess() {
        process(this.srcData);
    }
}
