package com.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

public class TT {
    public static void main(String[] args) {
        Integer a = new Integer(4);
        Integer b = 4;
        Integer c = Integer.valueOf(4);

        pr(a == b);     // 1     // 0
        pr(a.equals(b));   // 0     // 1

        pr(a == c);     // 1
        pr(a.equals(c));   // 1

        pr(b == c);     // 1
        pr(b.equals(c));   // 0

        HashMap p;
        ArrayList l;
        Hashtable t;
    }

    public static void pr(Object o) {
        System.out.println(o);
    }
}
