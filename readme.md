PDF2HTML2JSON
==

将 PDF 转换为 HTML 格式，并筛选表格，最终将表格数据转换为 JSON 文档，提供给 NoSQL 数据库使用

2018-03-29
--
提供 Table 类，传入一段 html 文本即可转换为 plain text 和 json

#### 转换规则：
    
根据 `div` 标签 `<div class="c x1 yc8 w25 h2d"></div>` 识别 `class` 属性的值；<br>

若 `class` 值为 `c` 则表示符合「表格」的定义，再根据 `y*` 判断筛选出每一行的数据；<br>

确认表格为 `n * m` 即可开始解析转化，目前只支持第一行为 `表头`，第一列为 `key`，后续需要根据表格种类进行扩充规则；<br>

####实例：

原数据截图：
![GitLab](https://gitlab.com/slowfish/pdf2html2json/raw/master/src/main/resources/20180329/Screen%20Shot%202018-03-29%20at%205.47.19%20AM.png)


最终打印结果：

    {
       "总资产":
       {
          "2017 年 3 月 31 日/ 2017 年 1-3 月":"5,773,318",
          "同比（%）":"3.5",
          "2016 年 12 月 31 日/ 2016 年 1-3 月":"5,576,903",
          "主要变动原因":"业务增长"
       },
       "总负债":
       {
          "2017 年 3 月 31 日/ 2017 年 1-3 月":"5,254,793",
          "同比（%）":"3.2",
          "2016 年 12 月 31 日/ 2016 年 1-3 月":"5,090,442",
          "主要变动原因":"业务增长"
       },
       "归属于母公司股东权益":
       {
          "2017 年 3 月 31 日/ 2017 年 1-3 月":"411,477",
          "同比（%）":"7.3",
          "2016 年 12 月 31 日/ 2016 年 1-3 月":"383,449",
          "主要变动原因":"经营业绩贡献"
       }
    }
    
最终打印结果截图：
![GitLab](https://gitlab.com/slowfish/pdf2html2json/raw/master/src/main/resources/20180329/Screen%20Shot%202018-03-29%20at%205.46.41%20AM.png)

2018-04-05
--
可将整文档数据重组为表格列表，即 `List<Table>` ，支持多页同表

支持 SimpleTable 表格的转换，生成 json 文档

Table 至少包含字段：`startPageNumber` 、`endPageNumber` 、`title` 、`List<Element[]>`

转录出来的数据实例：
    
    {
       "归属于上市公司股东的扣除非经常性损益 的净利润（元）":
       {
          "2015 年":"126,718,112.81",
          "2016 年":"197,018,784.38",
          "本年比上年增减":"100.78%",
          "2017 年":"395,566,664.38"
       },
       "":
       {
          "2015 年":"2015 年末",
          "2016 年":"2016 年末",
          "本年比上年增减":"本年末比上年末增减",
          "2017 年":"2017 年末"
       },
       "总资产（元）":
       {
          "2015 年":"2,556,676,265.72",
          "2016 年":"3,116,436,683.10",
          "本年比上年增减":"44.86%",
          "2017 年":"4,514,530,800.80"
       },
       "营业收入（元）":
       {
          "2015 年":"1,489,071,485.32",
          "2016 年":"1,982,302,598.97",
          "本年比上年增减":"55.55%",
          "2017 年":"3,083,547,891.99"
       },
       "经营活动产生的现金流量净额（元）":
       {
          "2015 年":"156,796,906.49",
          "2016 年":"184,861,778.96",
          "本年比上年增减":"96.36%",
          "2017 年":"362,985,774.62"
       },
       "稀释每股收益（元/股）":
       {
          "2015 年":"0.68",
          "2016 年":"0.40",
          "本年比上年增减":"92.50%",
          "2017 年":"0.77"
       },
       "加权平均净资产收益率":
       {
          "2015 年":"11.27%",
          "2016 年":"10.98%",
          "本年比上年增减":"7.13%",
          "2017 年":"18.11%"
       },
       "归属于上市公司股东的净利润（元）":
       {
          "2015 年":"138,453,579.30",
          "2016 年":"208,347,320.80",
          "本年比上年增减":"92.93%",
          "2017 年":"401,967,285.28"
       },
       "基本每股收益（元/股）":
       {
          "2015 年":"0.68",
          "2016 年":"0.40",
          "本年比上年增减":"92.50%",
          "2017 年":"0.77"
       }
    
    }
    
下一步，等搭建好 MongoDB 再将数据持久化